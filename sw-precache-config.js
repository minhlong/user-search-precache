module.exports = {
  navigateFallback: '/index.html',
  stripPrefix: 'dist',
  root: 'dist/',
  staticFileGlobs: [
    'dist/index.html',
    'dist/**.js',
    'dist/**.css',
  ],
  runtimeCaching: [{
    urlPattern: /^https:\/\/api\.github\.com\/search.*/,
    handler: 'networkFirst'
  }]
};
import { Component, OnInit } from '@angular/core';
import { AppService } from '../service/getdata';
@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  providers: [AppService],
  styleUrls: ['./home-component.component.css'],
})
export class HomeComponentComponent implements OnInit {
  users: any[];
  list: any[];
  selectUser: any
  isTag: boolean
  tags: any[]
  search: string
  tagId: number
  heads: string[]
  listSearch: string[]
  isTypeHead: boolean
  tmp_tag: string
  public notifications = []
  notSearch: boolean
  listCTags: any[]
  newTag: string
  editId: number
  constructor(private appService: AppService) {
  }
  ngOnInit() {
    this.appService.getUsers().subscribe(data => {
      this.users = data
      this.list = data
      this.selectUser = data[0]
    })
    this.appService.getTags().subscribe(data => {
      this.listCTags = data
      this.tags = data
    })
    this.notSearch = false
    this.list = []
    this.isTag = false
    this.search = ''
    this.tagId = -1
    this.heads = []
    this.isTypeHead = false
    this.tmp_tag = ''
    this.newTag = ''
    this.listCTags = []
    this.editId = -1
    this.listSearch = ['FirstName', 'SureName', 'CompanyName']
  }
  setUser(user: any) {
    this.selectUser = user
  }
  searchTag(id: number) {
    this.isTag = false
    var tag
    this.listCTags.forEach((a) => { if (a != null && a.TagID == id) { tag = a } })
    this.search = '#' + tag.Name
    this.appService.searchTag(this.search).subscribe(data1 => {
      this.users = data1
      this.list = data1
      this.selectUser = null
    })
  }
  searchText(not: boolean, str: string) {
    this.tagId = -1
    if (str.indexOf('#') == 0) {
      this.isTag = true
      var str_se = str.substring(1)
      str_se = str_se ? str_se : ''
      this.listCTags = this.tags.filter((a) => { return a != null && ((new RegExp('^' + str_se, 'i')).test(a.Name)) })
    }
    else {
      this.isTag = false
      this.appService.search(str).subscribe(data1 => {
        this.users = data1
        this.list = data1
        this.selectUser = null
      })
    }
    this.isTypeHead = false
    this.notSearch = false
  }
  setTag(id: number) {
    if (this.tagId != id) {
      this.tagId = id
    }
    else {
      this.tagId = -1
    }
  }
  checkUnique(list: any[], field: string, id: number) {
    return list.some((a) => { return a != null && a[field] == id })
  }
  modifyUser(user: any) {
    var index = -1
    this.list.forEach((a, index) => { if (a.ContactID == user.ContactID) { this.list[index] = user } })
  }
  addTag(user: any, tag: any) {
    if (user.CTags.some((a) => { return a != null && a.TagID == tag.TagID })) {
      this.appService.removeTagContact(user.ContactID, tag.TagID).subscribe(data => {
        this.appService.getUser(user.ContactID).subscribe(data => {
          this.modifyUser(data)
        })
      })
    }
    else {
      this.appService.addTagContact(user.ContactID, tag.TagID).subscribe(data => {
        this.appService.getUser(user.ContactID).subscribe(data => {
          this.modifyUser(data)
        })
      })
    }
  }
  addNewTag(str: string) {
    if (str != '') {
      var tag = this.tags.length > 0 ? this.tags[this.tags.length - 1] : null
      var id = tag ? tag.TagID : 0
      while (!this.checkUnique(this.tags, 'TagID', id)) {
        id++
      }
      var new_tag = { TagID: id, Name: str }
      this.appService.addTag(new_tag).subscribe(data => {
        this.appService.getTags().subscribe(data => {
          this.tags = data
        })
      })
    }
  }
  deleteTag(id: number) {
    this.appService.deleteTag(id).subscribe(data => {
      this.appService.getTags().subscribe(data1 => {
        this.tags = data1
        this.appService.getUsers().subscribe(data2 => {
          this.users = data2
          this.list = data2
        })
      })
    })
    this.list = this.users
  }
  saveTag(str: string, id: number) {
    if (str != '') {
      var new_tag = { TagID: id, Name: str }
      this.appService.editTag(new_tag).subscribe(data0 => {
        this.appService.getTags().subscribe(data => {
          this.tags = data
          this.appService.getUsers().subscribe(data1 => {
            this.users = data1
            this.list = this.list.map((item) => {
              return data1.find((a) => {
                return a.ContactID == item.ContactID
              });
            })
          })
        })
      })
      this.editId = id;
    }
  }
  checkX(tag_list: any[], tag: any) {
    var check = tag_list ? tag_list.some((a) => { return a != null && tag != null && a.TagID == tag.TagID }) : false
    if (check) {
      return true
    }
    else {
      return false
    }
  }
  closetag(event: any) {
    var target = event.target;
    if (!target.closest(".list-tag")) {
      this.tagId = -1;
    }
  }
  unreadNotificationCount() {
    return this.notifications.filter(notice => notice.read == false).length;
  }
}

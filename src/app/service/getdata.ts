import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AppService {
    constructor(private http: Http) { }
    search(str: string) {
        return this.http.get(`http://112.109.90.115:8082/api/Search?text=${str}`)
            .map(res => { console.log(res.json()); return res.json().Results })
            .catch(this.handleError)
    }
    searchTag(str: string) {
        str = str.replace('#', 'tag|')
        return this.http.get(`http://112.109.90.115:8082/api/Search?text=${str}`)
            .map(res => { console.log(res.json()); return res.json().Results })
            .catch(this.handleError)
    }
    getUsers() {
        return this.http.get(`http://112.109.90.115:8082/api/Contacts`)
            .map(res => res.json())
            .catch(this.handleError)
    }
    getUser(user_id: number) {
        return this.http.get(`http://112.109.90.115:8082/api/Contacts/` + user_id)
            .map(res => res.json())
            .catch(this.handleError)
    }
    getTags() {
        return this.http.get(`http://112.109.90.115:8082/api/Tags`)
            .map(res => res.json())
            .catch(this.handleError)
    }
    addTag(tag: any) {
        return this.http.post(`http://112.109.90.115:8082/api/Tags`, tag)
            .map(res => res.json())
            .catch(this.handleError)
    }
    editTag(tag: any) {
        return this.http.put(`http://112.109.90.115:8082/api/Tags/` + tag.TagID, tag)
            .catch(this.handleError)
    }
    deleteTag(tag_id: number) {
        return this.http.delete(`http://112.109.90.115:8082/api/Tags/` + tag_id)
            .catch(this.handleError)
    }
    addTagContact(user_id: number, tag_id: number) {
        return this.http.post(`http://112.109.90.115:8082/api/Contacts/` + user_id + '/Tags/' + tag_id, {})
            .map(res => res.json())
            .catch(this.handleError)
    }
    removeTagContact(user_id: number, tag_id: number) {
        return this.http.delete(`http://112.109.90.115:8082/api/Contacts/` + user_id + '/Tags/' + tag_id)
            .catch(this.handleError)
    }

    private handleError(error) {
        return Observable.throw(error.json().error)
    }

}